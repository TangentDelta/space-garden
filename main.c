#include "main.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int WORLD_WIDTH = 16;
const int WORLD_HEIGHT = 16;
const int SCREEN_BPP = 32;
const int FRAMES_PER_SECOND = 20;

int frames = 0;
bool cap = true;

char tileNames[32][16] = {{}};
char world[32][32] = {{}};
char entities[128][4] = {{}};

FILE *CONFIG_RESOURCES;
SDL_Surface* image = NULL;
SDL_Surface* fontsheet = NULL;
SDL_Surface* tilesheet = NULL;
SDL_Surface* selectionsheet = NULL;
SDL_Surface* screen = NULL;
SDL_Surface* scratchpad = NULL;
SDL_Surface* selectionsurface = NULL;
SDL_PixelFormat* fmt;

SDL_Event event;

int main(int argc,char* args[])
{
    char readChar;
    int readX = 0;
    int readY = 0;
    int readMode = 1;
    char readArray[64][16] = {{}};

    int cameraValues[3] = {0, 0, 1};
    int selectionValues[3] = {0,0,0};
    char selectionBlink = 0;

    bool Dead = false;
    bool dragging = false;

    SDL_Rect crop;
    SDL_Rect selectionCrop;

    crop.w = 29; //13
    crop.h = 32; //14
    crop.x = 0;
    crop.y = 0;

    selectionCrop.w = 29;
    selectionCrop.h = 32;
    selectionCrop.x = 0;
    selectionCrop.y = 0;

    int currentTile; //The current tile's ID
    int currentTileIndex; //The current tile on the world getting processed.
    int tileXPos = 0;
    int tileYPos = 0;

    for(int i = 0; i < 32; i++)
    {
        for(int j = 0; j < 32; j++)
        {
            world[i][j] = rand() % 3;
        }
    }
    world[0][0]=0;

    CONFIG_RESOURCES = fopen("resources.txt", "r");
    if(CONFIG_RESOURCES == NULL)
    {
        perror("Error while opening the resources file.\n");
        exit(EXIT_FAILURE);
    }

    while((readChar = fgetc(CONFIG_RESOURCES)) != EOF)
    {
        if(readChar == 0x0A)
        {
            readY = 0;
            readX++;
        }
        else
        {
            readArray[readX][readY] = readChar;
            readY++;
        }
    }
    readX = 0;
    readY = 0;
    while(readMode != 0)
    {
        if(strncmp(readArray[readX], "tiles:", 16))
        {
            readMode = 2;
        }

        if(readArray[readX][0] == 0)
        {
            readMode = 0;
        }

        if(readMode == 2)
        {
            for(int i = 0; i < 16; i++)
            {
                tileNames[readY][i] = readArray[readX][i];
            }
            readY++;
        }
        readX++;
    }

    SDL_Init(SDL_INIT_EVERYTHING);

    screen = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP,SDL_SWSURFACE);
    image = SDL_CreateRGBSurface(0, SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP, 0, 0, 0, 0);
    selectionsurface = SDL_CreateRGBSurface(0, SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP, 0, 0, 0, 0);
    scratchpad = SDL_CreateRGBSurface(0, SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP, 0, 0, 0, 0);
    fontsheet = SDL_LoadBMP("fontsheet.bmp");
    tilesheet = SDL_LoadBMP("tilesheet.bmp");
    selectionsheet = SDL_LoadBMP("toolSelection.bmp");
    fmt = screen->format;
    SDL_SetAlpha(tilesheet, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);
    SDL_SetAlpha(selectionsheet, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);
    SDL_SetAlpha(selectionsurface, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);
    SDL_SetAlpha(scratchpad, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);

    tilesheet->format->Amask = 0xFF000000;
    tilesheet->format->Ashift = 24;
    selectionsheet->format->Amask = 0xFF000000;
    selectionsheet->format->Ashift = 24;
    selectionsurface->format->Amask = 0xFF000000;
    selectionsurface->format->Ashift = 24;
    scratchpad->format->Amask = 0xFF000000;
    scratchpad->format->Ashift = 24;

    printf("Scratchpad alpha mask: %X\n",scratchpad->format->Amask);
    printf("Selection surface alpha mask: %X\n",selectionsurface->format->Amask);
    printf("Tile surface alpha mask: %X\n",tilesheet->format->Amask);
    printf("Image surface alpha mask: %X\n",image->format->Amask);

    while(!Dead)
    {
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT)
            {

                Dead = true;
            }
            else if(event.type == SDL_MOUSEBUTTONDOWN)
            {
                if(event.button.button == SDL_BUTTON_WHEELUP)
                {
                    cameraValues[2]+=(cameraValues[2] == 12 ? 0 : 1);
                }
                else if(event.button.button == SDL_BUTTON_WHEELDOWN)
                {
                    cameraValues[2]-=(cameraValues[2] == 1 ? 0 : 1);
                }
                else if(event.button.button == SDL_BUTTON_LEFT)
                {
                    selectionValues[2] = getpixel(selectionsurface,event.button.x,event.button.y)-0xFF000000;
                    printf("Mouse was clicked on tile %X.\n",selectionValues[2]);
                    dragging = true;
                }
            }
            else if(event.type == SDL_MOUSEBUTTONUP)
            {
                if(event.button.button == SDL_BUTTON_LEFT)
                {
                    dragging = false;
                }
            }
            else if(event.type == SDL_MOUSEMOTION)
            {
                if(dragging)
                {
                    cameraValues[0]+= event.motion.xrel / cameraValues[2];
                    cameraValues[1]+= event.motion.yrel / cameraValues[2];
                }
            }
        }

        SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0x00, 0x00, 0x00 ));
        crop.w = 29; //13
        crop.h = 32; //14
        currentTileIndex = 0;
        for(int heightIndex = 0; heightIndex < sizeof(world[0]); heightIndex++)
        {
            for(int widthIndex = 0; widthIndex < sizeof(world[0]); widthIndex++)
            {
                currentTile = world[heightIndex][widthIndex];
                crop.x = ((currentTile%8)*(29))+((currentTile%8)*3);
                crop.y = currentTile/8;
                tileXPos = (widthIndex*14)-(heightIndex*14);
                tileYPos = (heightIndex*7)+(widthIndex*7);
                apply_surface(tileXPos + cameraValues[0], tileYPos + cameraValues[1], tilesheet, image, &crop);

                //SDL_FillRect(scratchpad, NULL, 0x00000000);
                apply_surface(0,0,tilesheet,scratchpad, &crop);
                set_mask(scratchpad,0,(Uint32)currentTileIndex, &selectionCrop);

                apply_surface(tileXPos + cameraValues[0], tileYPos + cameraValues[1], scratchpad, selectionsurface, &selectionCrop);

                if(currentTileIndex==selectionValues[2])
                {
                    apply_surface(tileXPos + cameraValues[0],tileYPos + cameraValues[1],selectionsheet,image,&selectionCrop);
                }
                currentTileIndex++;
            }
        }

        selectionBlink++;

        //SDL_BlitSurface(selectionsurface,NULL,image,NULL);
        SDL_BlitSurface(image,NULL,screen,NULL);
        scale_surface(screen,0,0,cameraValues[2]);
        scale_surface(selectionsurface,0,0,cameraValues[2]);
        SDL_Flip(screen);

        //if(cap && (frames < 1000/FRAMES_PER_SECOND))
        //{selectionMaskHeight
            SDL_Delay(10);
        //}
        //frames++;
    }

    SDL_FreeSurface(image);
    SDL_FreeSurface(tilesheet);
    SDL_FreeSurface(fontsheet);

    SDL_Quit();

    return 0;
}
