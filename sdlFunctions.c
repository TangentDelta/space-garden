#include"sdlFunctions.h"

SDL_Surface *load_image( char* filename )
{
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;
	loadedImage = SDL_LoadBMP(filename);
	if(loadedImage != NULL)
	{
		optimizedImage = SDL_DisplayFormat(loadedImage);
		SDL_FreeSurface(loadedImage);
        if( optimizedImage != NULL )
        {
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
	}
	return optimizedImage;
}

void setpixel(SDL_Surface *surface,int x,int y,Uint32 pixel)
{
    Uint32 *pixels = (Uint32*)surface->pixels;
	pixels[ ( y * surface->w ) + x ] = pixel;
}

Uint32 getpixel(SDL_Surface *s, int x, int y)
{
    return ((unsigned int*)s->pixels)[y*(s->pitch/sizeof(unsigned int)) + x];
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, clip, destination, &offset );
}

void scale_surface(SDL_Surface *surface, int xOffset, int yOffset, int scale)
{
    SDL_Surface* scaledSurface = NULL;
    //Uint32 pixel = 0;

    if(surface != NULL)
    {
        scaledSurface = SDL_DisplayFormat(surface);
        for(int w=0; w < scaledSurface->w; w++)
        {
            for(int h=0; h < scaledSurface->h; h++)
            {
                //pixel = getpixel(surface,w,h);
                setpixel(scaledSurface, w, h, getpixel(surface,w/scale,h/scale));
            }
        }
        SDL_BlitSurface(scaledSurface, NULL, surface, NULL);
    }
    SDL_FreeSurface(scaledSurface);
    //free(pixel);
}

void set_mask(SDL_Surface *surface, Uint32 exclude, Uint32 mask, SDL_Rect* clip)
{
    Uint32 pixel = 0;
    for(int w=clip->x; w < clip->w; w++)
    {
        for(int h=clip->y; h < clip->h; h++)
        {
            pixel = getpixel(surface,w,h);
            if((pixel!=exclude))
            {
                setpixel(surface,w,h,mask + 0xFF000000);
            }
            else
            {
                setpixel(surface,w,h,0);
            }
        }
    }
}
