#ifndef SDLFUNCTIONS_H
#define SDLFUNCTIONS_H
#include<stdio.h>
#include<string.h>
#include<SDL/SDL.h>
SDL_Surface *load_image( char* filename );
void setpixel(SDL_Surface *surface,int x,int y,Uint32 pixel);
Uint32 getpixel(SDL_Surface *s, int x, int y);
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
void testThatStuff();
void scale_surface(SDL_Surface *surface, int xOffset, int yOffset, int scale);
void set_mask(SDL_Surface *surface, Uint32 exclude, Uint32 mask, SDL_Rect* clip);
#endif
